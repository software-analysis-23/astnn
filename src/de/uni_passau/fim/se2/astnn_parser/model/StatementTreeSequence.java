package de.uni_passau.fim.se2.astnn_parser.model;

import java.util.List;
import java.util.Locale;

import de.uni_passau.fim.se2.astnn_parser.model.ast.AstnnNode;

/**
 * A sequence of statement trees as required for the ASTNN model together with a label.
 *
 * @param label The identifying label for the statement trees, e.g., the method name.
 * @param statements The actual statements trees as defined in the ASTNN paper.
 */
public record StatementTreeSequence(String label, List<AstnnNode> statements) {

    /**
     * The label for the sequence normalised to only contain lower case letters and no underscores.
     *
     * @return The normalised label.
     */
    public String label() {
        return label.toLowerCase(Locale.ROOT).replace("_", "");
    }
}
