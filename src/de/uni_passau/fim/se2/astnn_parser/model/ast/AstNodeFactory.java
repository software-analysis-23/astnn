package de.uni_passau.fim.se2.astnn_parser.model.ast;

import static de.uni_passau.fim.se2.astnn_parser.model.ast.AstnnNode.BLOCK_LABEL;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Preconditions;

public class AstNodeFactory {

    private static final AstLeaf BLOCK_NODE = new AstLeaf(BLOCK_LABEL);

    private static final AstLeaf BLOCK_END_NODE = new AstLeaf(StatementType.END);

    private AstNodeFactory() {
        throw new UnsupportedOperationException("utility class constructor");
    }

    public static AstnnNode build(final String label, final List<AstnnNode> children) {
        if (children.isEmpty()) {
            return new AstLeaf(label);
        }
        else {
            return new AstNode(label, children);
        }
    }

    public static AstnnNode build(final StatementType statementType, final List<AstnnNode> children) {
        if (children.isEmpty()) {
            return new AstLeaf(statementType);
        }
        else {
            return new AstNode(statementType, children);
        }
    }

    public static AstnnNode build(final String label, final StatementType statementType,
            final List<AstnnNode> children) {
        if (children.isEmpty()) {
            return new AstLeaf(label, statementType);
        }
        else {
            return new AstNode(label, statementType, children);
        }
    }

    public static AstLeaf block() {
        return BLOCK_NODE;
    }

    public static AstnnNode block(final List<AstnnNode> children) {
        if (children.isEmpty()) {
            return block();
        }
        else {
            return new AstNode(BLOCK_LABEL, children);
        }
    }

    public static AstnnNode blockEnd() {
        return BLOCK_END_NODE;
    }

    public static AstNode methodDeclaration(final String name, final AstnnNode returnType,
            final List<AstnnNode> modifiers,
            final List<AstnnNode> parameters, final List<AstnnNode> exceptions, final AstnnNode body) {
        Preconditions.checkArgument(BLOCK_LABEL.equals(body.getLabel()),
                "The block child of an AstNode must have the block label");

        final List<AstnnNode> children = new ArrayList<>();
        if (!modifiers.isEmpty()) {
            children.add(new AstNode("modifiers", modifiers));
        }
        children.add(new AstNode("type", returnType));
        if (!parameters.isEmpty()) {
            children.add(new AstNode("parameters", parameters));
        }
        if (!exceptions.isEmpty()) {
            children.add(new AstNode("exceptions", exceptions));
        }

        children.add(body);

        return new AstNode(name, StatementType.METHOD, children);
    }

    public static AstNode blockStatement(final StatementType statementType, final List<AstnnNode> headerChildren,
            final AstnnNode blockChild) {
        final List<AstnnNode> children = new ArrayList<>(headerChildren);
        children.add(blockChild);
        return new AstNode(statementType.name(), statementType, children);
    }

    public static AstNode blockStatement(final StatementType statementType, final AstnnNode blockChild) {
        Preconditions.checkArgument(BLOCK_LABEL.equals(blockChild.getLabel()),
                "The block child of an AstNode must have the block label");

        return new AstNode(statementType, blockChild);
    }
}
