package de.uni_passau.fim.se2.astnn_parser.model.ast;

import java.util.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Represents a node in the simplified abstract syntax tree.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public interface AstnnNode {

    String BLOCK_LABEL = "BLOCK";

    String getLabel();

    /**
     * Checks if the node has no further children.
     * <p>
     * Can be ignored for the JSON, as there the children list is just empty.
     *
     * @return Ture, if the node has no children.
     */
    @JsonIgnore
    boolean isLeaf();

    /**
     * Checks if the AST node is a statement.
     * <p>
     * Only included in the JSON if {@code true}.
     *
     * @return True, if the node is a statement.
     */
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    boolean isStatement();

    /**
     * Checks if the node has a block child, i.e., if it is a block-statement.
     *
     * @return True, if the node is a block-statement.
     */
    boolean hasBlock();

    List<AstnnNode> getChildren();

    /**
     * Turns a tree into a statement tree by removing all direct and transitive
     * children that are statements.
     *
     * @return A new AST node that does not contain statements further down in
     *         the tree.
     */
    AstnnNode asStatementTree();

}
