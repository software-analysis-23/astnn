package de.uni_passau.fim.se2.astnn_parser.model.ast;

import java.util.List;
import java.util.Objects;

import com.google.common.base.Preconditions;

/**
 * Represents an AST node that has children.
 */
public class AstNode implements AstnnNode {

    private final String label;

    private final StatementType statementType;

    private final List<AstnnNode> children;

    public AstNode(final String label, final List<AstnnNode> children) {
        this(label, null, children);
    }

    public AstNode(final StatementType statementType, final List<AstnnNode> children) {
        this(statementType.name(), statementType, children);
    }

    public AstNode(final String label, final AstnnNode child) {
        this(label, List.of(child));
    }

    public AstNode(final StatementType statementType, final AstnnNode child) {
        this(statementType.name(), statementType, List.of(child));
    }

    AstNode(final String label, final StatementType statementType, final List<AstnnNode> children) {
        Preconditions.checkArgument(!children.isEmpty(), "An AstNode must have at least one child");

        this.label = label;
        this.statementType = statementType;
        this.children = children;
    }

    @Override
    public String getLabel() {
        return this.label;
    }

    @Override
    public boolean isLeaf() {
        return false;
    }

    @Override
    public boolean isStatement() {
        return statementType != null;
    }

    public StatementType getStatementType() {
        return statementType;
    }

    @Override
    public boolean hasBlock() {
        return children.stream().map(AstnnNode::getLabel).anyMatch(AstnnNode.BLOCK_LABEL::equals);
    }

    @Override
    public List<AstnnNode> getChildren() {
        return children;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AstnnNode asStatementTree() {
        // TODO Implement me
        throw new UnsupportedOperationException("Implement me");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        else if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AstNode astNode = (AstNode) o;
        return label.equals(astNode.label) && children.equals(astNode.children);
    }

    @Override
    public int hashCode() {
        return Objects.hash(label, children, statementType);
    }

    @Override
    public String toString() {
        return "AstNode{" + "label='" + label + '\'' + ", statementType=" + statementType + ", children=" + children
                + '}';
    }
}
