package de.uni_passau.fim.se2.astnn_parser.model.ast;

import java.util.List;
import java.util.Objects;

/**
 * Represents an AST node that has no further children.
 */
public class AstLeaf implements AstnnNode {

    private static final String EMPTY = "<EMPTY>";

    public static final AstLeaf EMPTY_PLACEHOLDER = new AstLeaf(EMPTY);

    private final String label;

    private final StatementType statementType;

    public AstLeaf(final String label) {
        if (label.isEmpty()) {
            this.label = EMPTY;
        }
        else {
            this.label = label;
        }

        this.statementType = null;
    }

    public AstLeaf(final StatementType statementType) {
        this(statementType.name(), statementType);
    }

    AstLeaf(final String label, final StatementType statementType) {
        this.label = label;
        this.statementType = statementType;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public boolean isLeaf() {
        return true;
    }

    @Override
    public boolean isStatement() {
        return statementType != null;
    }

    @Override
    public boolean hasBlock() {
        return false;
    }

    @Override
    public List<AstnnNode> getChildren() {
        return List.of();
    }

    @Override
    public AstnnNode asStatementTree() {
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        else if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AstLeaf astLeaf = (AstLeaf) o;
        return label.equals(astLeaf.label) && statementType == astLeaf.statementType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(label, statementType);
    }

    @Override
    public String toString() {
        return "AstLeaf{" + "label='" + label + '\'' + ", statementType=" + statementType + '}';
    }
}
