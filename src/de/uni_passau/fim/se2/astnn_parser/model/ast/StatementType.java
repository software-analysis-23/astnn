package de.uni_passau.fim.se2.astnn_parser.model.ast;

/**
 * The various types of block and regular statements that can occur in the Java language.
 */
public enum StatementType {
    ASSERT,
    ASSIGNMENT,
    BREAK,
    CONSTRUCTOR_CALL,
    CONTINUE,
    EMPTY,
    EXPRESSION,
    LABELLED,
    METHOD_CALL,
    RETURN,
    THROW,
    VARIABLE_DECLARATION,
    YIELD,

    CASE,
    CATCH,
    DEFAULT,
    DO,
    ELSE,
    FINALLY,
    FOR,
    FOREACH,
    IF,
    LAMBDA,
    METHOD,
    SWITCH,
    SYNCHRONIZED,
    TRY,
    WHILE,

    // end-marker for a block, easier to add it as a statement rather than introducing special cases in other places
    END,
}
