package de.uni_passau.fim.se2.astnn_parser.pipeline;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.logging.Logger;
import java.util.stream.Stream;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javaparser.ast.CompilationUnit;

import de.uni_passau.fim.se2.astnn_parser.model.StatementTreeSequence;
import de.uni_passau.fim.se2.astnn_parser.parser.Parser;
import de.uni_passau.fim.se2.astnn_parser.pipeline.transform.FormatTransformer;

/**
 * The concrete transformer from Java source code into the format used by the ASTNN machine learning model.
 */
public class AstnnPipeline implements Pipeline {

    private final Logger logger = Logger.getLogger(this.getClass().getSimpleName());

    private final ObjectMapper objectMapper;

    private final FormatTransformer<CompilationUnit, Stream<StatementTreeSequence>> sequenceExtractor;

    public AstnnPipeline(final FormatTransformer<CompilationUnit, Stream<StatementTreeSequence>> sequenceExtractor) {
        this.objectMapper = new ObjectMapper();
        this.sequenceExtractor = sequenceExtractor;
    }

    @Override
    public void processDirectory(final Path sourceDirectory, final Path outputFile) throws IOException {
        final Parser parser = new Parser(sourceDirectory);
        final Stream<CompilationUnit> compilationUnits = parser.parseDirectory();
        logger.info("Parsing complete. Starting transformation.");
        final Stream<StatementTreeSequence> sequences = compilationUnits.flatMap(sequenceExtractor);

        try (var bw = Files.newOutputStream(outputFile); PrintWriter pw = new PrintWriter(bw)) {
            sequences.map(this::sequenceToLine).filter(s -> !s.isEmpty()).forEach(pw::println);
        }

        logger.info("Wrote result to " + outputFile);
    }

    private String sequenceToLine(final StatementTreeSequence sequence) {
        try {
            return objectMapper.writeValueAsString(sequence);
        }
        catch (JsonProcessingException ex) {
            // as long as the classes have got proper Jackson annotations this should never happen
            ex.printStackTrace();
            return "";
        }
    }
}
