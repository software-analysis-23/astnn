package de.uni_passau.fim.se2.astnn_parser.pipeline;

import de.uni_passau.fim.se2.astnn_parser.pipeline.transform.AstnnStatementSequenceTransformer;
import de.uni_passau.fim.se2.astnn_parser.pipeline.transform.CompilationUnitToAstnnTransformer;
import de.uni_passau.fim.se2.astnn_parser.pipeline.transform.MethodListExtractor;
import de.uni_passau.fim.se2.astnn_parser.pipeline.transform.ToAstTransformer;

/**
 * Combines the individual processing steps into a whole transformation pipeline.
 * <p>
 * The final pipeline performs all required intermediate steps to convert the Java source code found in a directory into
 * a single JsonL-file that contains the statement trees for the methods with one method per line.
 */
public class PipelineBuilder {

    private PipelineBuilder() {
    }

    public static Pipeline build() {
        final MethodListExtractor methodDeclarationExtractor = new MethodListExtractor();
        final ToAstTransformer astTransformer = new ToAstTransformer();
        final AstnnStatementSequenceTransformer sequenceTransformer = new AstnnStatementSequenceTransformer();
        final CompilationUnitToAstnnTransformer sequenceExtractor = new CompilationUnitToAstnnTransformer(
                methodDeclarationExtractor, astTransformer, sequenceTransformer);

        return new AstnnPipeline(sequenceExtractor);
    }
}
