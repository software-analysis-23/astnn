package de.uni_passau.fim.se2.astnn_parser.pipeline;

import java.io.IOException;
import java.nio.file.Path;

/**
 * Defines a processing pipeline that transforms some Java source code in the source directory into another format which
 * then will be written to a single output file.
 */
@FunctionalInterface
public interface Pipeline {

    /**
     * Performs all required intermediate steps to convert source code into a different format.
     *
     * @param sourceDirectory The folder that contains the source code.
     * @param outputFile The file into which the processed data should be written.
     * @throws IOException Thrown if some error during reading the input or writing the output occurs.
     */
    void processDirectory(final Path sourceDirectory, final Path outputFile) throws IOException;
}
