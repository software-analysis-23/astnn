package de.uni_passau.fim.se2.astnn_parser.pipeline.transform;

import java.util.stream.Stream;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.visitor.GenericVisitorWithDefaults;

/**
 * Finds all concrete method declarations in a compilation unit.
 */
public class MethodListExtractor implements FormatTransformer<CompilationUnit, Stream<MethodDeclaration>> {

    @Override
    public Stream<MethodDeclaration> apply(CompilationUnit compilationUnit) {
        final var visitor = new MethodListExtractionVisitor();
        return compilationUnit.accept(visitor, null);
    }

    private static class MethodListExtractionVisitor
            extends GenericVisitorWithDefaults<Stream<MethodDeclaration>, Void> {

        @Override
        public Stream<MethodDeclaration> defaultAction(Node n, Void arg) {
            return n.getChildNodes().stream().flatMap(s -> s.accept(this, arg));
        }

        @Override
        public Stream<MethodDeclaration> visit(MethodDeclaration n, Void arg) {
            // we are not interested in abstract methods
            if (n.getBody().isEmpty()) {
                return Stream.empty();
            }
            else {
                return Stream.of(n);
            }
        }
    }
}
