package de.uni_passau.fim.se2.astnn_parser.pipeline.transform;

import java.util.logging.Logger;

import com.github.javaparser.ast.ArrayCreationLevel;
import com.github.javaparser.ast.Modifier;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.Parameter;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.comments.BlockComment;
import com.github.javaparser.ast.comments.JavadocComment;
import com.github.javaparser.ast.comments.LineComment;
import com.github.javaparser.ast.expr.ArrayAccessExpr;
import com.github.javaparser.ast.expr.ArrayCreationExpr;
import com.github.javaparser.ast.expr.ArrayInitializerExpr;
import com.github.javaparser.ast.expr.AssignExpr;
import com.github.javaparser.ast.expr.BinaryExpr;
import com.github.javaparser.ast.expr.BooleanLiteralExpr;
import com.github.javaparser.ast.expr.CastExpr;
import com.github.javaparser.ast.expr.CharLiteralExpr;
import com.github.javaparser.ast.expr.ClassExpr;
import com.github.javaparser.ast.expr.ConditionalExpr;
import com.github.javaparser.ast.expr.DoubleLiteralExpr;
import com.github.javaparser.ast.expr.EnclosedExpr;
import com.github.javaparser.ast.expr.FieldAccessExpr;
import com.github.javaparser.ast.expr.InstanceOfExpr;
import com.github.javaparser.ast.expr.IntegerLiteralExpr;
import com.github.javaparser.ast.expr.LambdaExpr;
import com.github.javaparser.ast.expr.LongLiteralExpr;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.expr.MethodReferenceExpr;
import com.github.javaparser.ast.expr.Name;
import com.github.javaparser.ast.expr.NameExpr;
import com.github.javaparser.ast.expr.NullLiteralExpr;
import com.github.javaparser.ast.expr.ObjectCreationExpr;
import com.github.javaparser.ast.expr.PatternExpr;
import com.github.javaparser.ast.expr.SimpleName;
import com.github.javaparser.ast.expr.StringLiteralExpr;
import com.github.javaparser.ast.expr.SuperExpr;
import com.github.javaparser.ast.expr.SwitchExpr;
import com.github.javaparser.ast.expr.TextBlockLiteralExpr;
import com.github.javaparser.ast.expr.ThisExpr;
import com.github.javaparser.ast.expr.TypeExpr;
import com.github.javaparser.ast.expr.UnaryExpr;
import com.github.javaparser.ast.expr.VariableDeclarationExpr;
import com.github.javaparser.ast.stmt.AssertStmt;
import com.github.javaparser.ast.stmt.BlockStmt;
import com.github.javaparser.ast.stmt.BreakStmt;
import com.github.javaparser.ast.stmt.CatchClause;
import com.github.javaparser.ast.stmt.ContinueStmt;
import com.github.javaparser.ast.stmt.DoStmt;
import com.github.javaparser.ast.stmt.EmptyStmt;
import com.github.javaparser.ast.stmt.ExplicitConstructorInvocationStmt;
import com.github.javaparser.ast.stmt.ExpressionStmt;
import com.github.javaparser.ast.stmt.ForEachStmt;
import com.github.javaparser.ast.stmt.ForStmt;
import com.github.javaparser.ast.stmt.IfStmt;
import com.github.javaparser.ast.stmt.LabeledStmt;
import com.github.javaparser.ast.stmt.LocalClassDeclarationStmt;
import com.github.javaparser.ast.stmt.LocalRecordDeclarationStmt;
import com.github.javaparser.ast.stmt.ReturnStmt;
import com.github.javaparser.ast.stmt.SwitchEntry;
import com.github.javaparser.ast.stmt.SwitchStmt;
import com.github.javaparser.ast.stmt.SynchronizedStmt;
import com.github.javaparser.ast.stmt.ThrowStmt;
import com.github.javaparser.ast.stmt.TryStmt;
import com.github.javaparser.ast.stmt.UnparsableStmt;
import com.github.javaparser.ast.stmt.WhileStmt;
import com.github.javaparser.ast.stmt.YieldStmt;
import com.github.javaparser.ast.type.ArrayType;
import com.github.javaparser.ast.type.ClassOrInterfaceType;
import com.github.javaparser.ast.type.IntersectionType;
import com.github.javaparser.ast.type.PrimitiveType;
import com.github.javaparser.ast.type.UnionType;
import com.github.javaparser.ast.type.UnknownType;
import com.github.javaparser.ast.type.VarType;
import com.github.javaparser.ast.type.VoidType;
import com.github.javaparser.ast.type.WildcardType;
import com.github.javaparser.ast.visitor.GenericVisitorWithDefaults;
import de.uni_passau.fim.se2.astnn_parser.model.ast.AstLeaf;
import de.uni_passau.fim.se2.astnn_parser.model.ast.AstNode;
import de.uni_passau.fim.se2.astnn_parser.model.ast.AstnnNode;
import de.uni_passau.fim.se2.astnn_parser.model.ast.StatementType;

/**
 * Visits a compilation unit to transform the Javaparser-AST into a simpler format that can be
 * worked with during the following processing steps.
 */
public class ToAstTransformVisitor extends GenericVisitorWithDefaults<AstnnNode, Void> {

    private final Logger logger = Logger.getLogger(ToAstTransformVisitor.class.getSimpleName());

    // Labels to be used for the nodes of the ST-tree.
    private static final String OBJECT_CREATION_EXPR_SCOPE = "scope",
            PATTERN_EXPR_NAME = "name",
            PARAMETER_NAME = "name",
            DO_STMT_CONDITION = "condition",
            FOR_STMT_VARIABLE = "variable",
            FOR_STMT_ITERABLE = "iterable",
            ANONYMOUS_CLASS_BODY = "<ANONYMOUS_CLASS_BODY>",
            SWITCH_CASES = "cases",
            ARRAY_CREATION_EXPR_INIT = "init",
            FOR_STMT_INIT = "init",
            METHOD_CALL_EXPR_NAME = "name",
            LAMBDA_PARAMETERS = "parameters",
            METHOD_CALL_EXPR_SCOPE = "scope",
            FIELD_ACCESS_EXPR_SCOPE = "scope",
            SWITCH_SELECTOR = "selector",
            FOR_STMT_COMPARE = "compare",
            FOR_STMT_UPDATE = "update",
            SWITCH_ENTRY_LABELS = "labels",
            TRY_STMT = "try",
            IF_STMT_CONDITION = "condition",
            IF_STMT = "if",
            TRY_STMT_RESOURCES = "resources",
            SYNCHRONIZED_STMT_EXPRESSION = "expression",
            LABELED_STMT_LABEL = "label",
            VAR_DECL_MODIFIERS = "modifiers",
            ARRAY_CREATION_DIMENSION = "dimension",
            SUPER_EXPR = "super",
            THIS_EXPR = "this",
            UNKNOWN_TYPE = "<UNKNOWN>",
            VOID_TYPE = "void",
            WILDCARD_TYPE = "?",
            VAR_TYPE = "var",
            NULL_LITERAL = "null",
            PARAMETER_TYPE = "type",
            PATTERN_EXPR_TYPE = "type",
            ARRAY_CREATION_LEVEL_EMPTY = "[]",
            ARRAY_CREATION_LEVEL_NON_EMPTY = "arrayDimension",
            METHOD_ARGUMENTS = "arguments",
            VAR_DECL_INIT = "init";

    // region Block Statements

    @Override
    public AstNode visit(MethodDeclaration n, Void arg) {
        // TODO implement me
        throw new UnsupportedOperationException("Implement me");
    }

    @Override
    public AstnnNode visit(BlockStmt n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstNode visit(DoStmt n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstnnNode visit(ForEachStmt n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstnnNode visit(ForStmt n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstNode visit(IfStmt n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstnnNode visit(SwitchStmt n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstnnNode visit(SwitchEntry n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstNode visit(SynchronizedStmt n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstnnNode visit(TryStmt n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstNode visit(WhileStmt n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstNode visit(CatchClause n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    // endregion Block Statements

    // region Statements

    @Override
    public AstnnNode visit(AssertStmt n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstnnNode visit(BreakStmt n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstnnNode visit(ContinueStmt n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstLeaf visit(EmptyStmt n, Void arg) {
        return new AstLeaf(StatementType.EMPTY);
    }

    @Override
    public AstnnNode visit(ExplicitConstructorInvocationStmt n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstNode visit(ExpressionStmt n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstNode visit(LabeledStmt n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstnnNode visit(ReturnStmt n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstNode visit(ThrowStmt n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstLeaf visit(LocalClassDeclarationStmt n, Void arg) {
        return new AstLeaf(n.getClass().getSimpleName());
    }

    @Override
    public AstLeaf visit(LocalRecordDeclarationStmt n, Void arg) {
        return new AstLeaf(n.getClass().getSimpleName());
    }

    @Override
    public AstnnNode visit(UnparsableStmt n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstNode visit(YieldStmt n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    // endregion Statements

    // region Expressions

    @Override
    public AstNode visit(BinaryExpr n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstNode visit(UnaryExpr n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstNode visit(VariableDeclarationExpr n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstNode visit(VariableDeclarator n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstnnNode visit(ArrayAccessExpr n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstnnNode visit(ArrayCreationExpr n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstnnNode visit(ArrayInitializerExpr n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstnnNode visit(AssignExpr n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstnnNode visit(CastExpr n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstnnNode visit(ClassExpr n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstnnNode visit(ConditionalExpr n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstnnNode visit(EnclosedExpr n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstnnNode visit(FieldAccessExpr n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstnnNode visit(InstanceOfExpr n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstnnNode visit(MethodCallExpr n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstnnNode visit(ObjectCreationExpr n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstnnNode visit(SuperExpr n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstnnNode visit(ThisExpr n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstNode visit(LambdaExpr n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstnnNode visit(MethodReferenceExpr n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstLeaf visit(TypeExpr n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstnnNode visit(SwitchExpr n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstNode visit(PatternExpr n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    // endregion Expressions

    // region Types and Names

    @Override
    public AstLeaf visit(SimpleName n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstLeaf visit(NameExpr n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstLeaf visit(Name n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstLeaf visit(ClassOrInterfaceType n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstLeaf visit(PrimitiveType n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstLeaf visit(ArrayType n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstnnNode visit(IntersectionType n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstnnNode visit(UnionType n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstLeaf visit(UnknownType n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstLeaf visit(VoidType n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstLeaf visit(WildcardType n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstLeaf visit(VarType n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    // endregion Types and Names

    // region Literals

    @Override
    public AstLeaf visit(BooleanLiteralExpr n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstLeaf visit(CharLiteralExpr n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstLeaf visit(DoubleLiteralExpr n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstLeaf visit(IntegerLiteralExpr n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstLeaf visit(LongLiteralExpr n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstLeaf visit(NullLiteralExpr n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstLeaf visit(StringLiteralExpr n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstLeaf visit(TextBlockLiteralExpr n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    // endregion Literals

    @Override
    public AstLeaf visit(Modifier n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstnnNode visit(ArrayCreationLevel n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstnnNode visit(Parameter n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    // region Comments

    @Override
    public AstnnNode visit(JavadocComment n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstnnNode visit(BlockComment n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    @Override
    public AstnnNode visit(LineComment n, Void arg) {
        // TODO Implement me if necessary
        return null;
    }

    // endregion Comments
}
