package de.uni_passau.fim.se2.astnn_parser.pipeline.transform;

import com.github.javaparser.ast.body.MethodDeclaration;

import de.uni_passau.fim.se2.astnn_parser.model.ast.AstNode;

/**
 * Transforms method declarations into the simplified AST format using the {@link ToAstTransformVisitor}.
 */
public class ToAstTransformer implements FormatTransformer<MethodDeclaration, AstNode> {

    private final ToAstTransformVisitor visitor = new ToAstTransformVisitor();

    @Override
    public AstNode apply(MethodDeclaration methodDeclaration) {
        return visitor.visit(methodDeclaration, null);
    }
}
