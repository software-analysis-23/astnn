package de.uni_passau.fim.se2.astnn_parser.pipeline.transform;

import java.util.function.Function;

/**
 * Type-alias for a function that takes some input of type I and returns some
 * output O.
 *
 * @param <I> The type of the input that should be processed.
 * @param <O> The type of the processed output.
 */
@FunctionalInterface
public interface FormatTransformer<I, O> extends Function<I, O> {
}
