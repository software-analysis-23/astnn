package de.uni_passau.fim.se2.astnn_parser.pipeline.transform;

import java.util.stream.Stream;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.MethodDeclaration;

import de.uni_passau.fim.se2.astnn_parser.model.StatementTreeSequence;
import de.uni_passau.fim.se2.astnn_parser.model.ast.AstNode;

/**
 * Transforms a parsed Java file into the statement tree sequences of the methods in this file.
 */
public class CompilationUnitToAstnnTransformer
        implements FormatTransformer<CompilationUnit, Stream<StatementTreeSequence>> {

    private final FormatTransformer<CompilationUnit, Stream<MethodDeclaration>> methodDeclarationExtractor;

    private final FormatTransformer<MethodDeclaration, AstNode> astTransformer;

    private final FormatTransformer<AstNode, StatementTreeSequence> astnnStatementSequenceTransformer;

    public CompilationUnitToAstnnTransformer(
            final FormatTransformer<CompilationUnit, Stream<MethodDeclaration>> methodDeclarationExtractor,
            final FormatTransformer<MethodDeclaration, AstNode> astTransformer,
            final FormatTransformer<AstNode, StatementTreeSequence> astnnStatementSequenceTransformer) {
        this.methodDeclarationExtractor = methodDeclarationExtractor;
        this.astTransformer = astTransformer;
        this.astnnStatementSequenceTransformer = astnnStatementSequenceTransformer;
    }

    /**
     * Transforms a single Java file into the statement tree sequences of the methods in this file.
     *
     * @param compilationUnit A parsed Java file.
     * @return One statement tree sequence per method in this file.
     */
    @Override
    public Stream<StatementTreeSequence> apply(CompilationUnit compilationUnit) {
        // TODO Implement me
        throw new UnsupportedOperationException("Implement me");
    }
}
