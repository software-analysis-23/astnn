package de.uni_passau.fim.se2.astnn_parser.pipeline.transform;

import java.util.List;

import com.google.common.base.Preconditions;
import de.uni_passau.fim.se2.astnn_parser.model.StatementTreeSequence;
import de.uni_passau.fim.se2.astnn_parser.model.ast.AstNode;
import de.uni_passau.fim.se2.astnn_parser.model.ast.AstnnNode;
import de.uni_passau.fim.se2.astnn_parser.model.ast.StatementType;

/**
 * Transforms block statements of type {@link StatementType#METHOD} into statement tree sequences.
 * <p>
 * A statement tree sequence is a flat list of all statements contained in the method and its
 * nested structures. Each statement tree is a part of the AST that contains a statement as its
 * root node with no other statement nodes further down in this subtree.
 */
public class AstnnStatementSequenceTransformer implements FormatTransformer<AstNode, StatementTreeSequence> {

    @Override
    public StatementTreeSequence apply(AstNode input) {
        Preconditions.checkArgument(StatementType.METHOD.equals(input.getStatementType()));

        List<AstnnNode> statements = toStatementSequence(input);
        return new StatementTreeSequence(input.getLabel(), statements);
    }

    /**
     * Performs the actual splitting of the full AST of the method into separate trees for each of
     * the statements.
     * <p>
     * Protected access to allow for easier testing.
     *
     * @param node The root node of some tree that should be split.
     * @return A list of statement trees.
     */
    List<AstnnNode> toStatementSequence(final AstnnNode node) {
        // TODO implement me
        // Testing the build pipeline
        throw new UnsupportedOperationException("Implement me");
    }
}
