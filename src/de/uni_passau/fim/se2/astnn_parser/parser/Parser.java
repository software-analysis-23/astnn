package de.uni_passau.fim.se2.astnn_parser.parser;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.github.javaparser.ParseResult;
import com.github.javaparser.Problem;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.symbolsolver.JavaSymbolSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.CombinedTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.JavaParserTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.ReflectionTypeSolver;
import com.github.javaparser.utils.SourceRoot;

/**
 * Convenience wrapper around the parser of the JavaParser library.
 */
public class Parser {

    private final Logger logger = Logger.getLogger(this.getClass().getSimpleName());

    private final List<SourceRoot> sourceRoots;

    /**
     * Creates a new parser that can parse the given source folder.
     * <p>
     * The parser is specific for each source folder to enable proper type/class resolution.
     *
     * @param sourceDirectory A folder that contains a Java package structure. For Maven/Gradle projects this is
     *                        typically {@code src/main/java/}.
     */
    public Parser(final Path sourceDirectory) {
        sourceRoots = List.of(new SourceRoot(sourceDirectory));

        CombinedTypeSolver combinedTypeSolver = new CombinedTypeSolver();
        for (SourceRoot s : sourceRoots) {
            combinedTypeSolver.add(new JavaParserTypeSolver(s.getRoot()));
        }
        combinedTypeSolver.add(new ReflectionTypeSolver());
        JavaSymbolSolver symbolResolver = new JavaSymbolSolver(combinedTypeSolver);

        sourceRoots.forEach(s -> s.getParserConfiguration().setSymbolResolver(symbolResolver));
    }

    /**
     * Performs the actual parsing.
     * <p>
     * Skips compilation units that cannot be parsed and continues parsing the remaining ones.
     *
     * @return All compilation units int the source directory which could be parsed.
     */
    public Stream<CompilationUnit> parseDirectory() {
        return sourceRoots.stream().flatMap(s -> {
            try {
                logger.info("Parsing Java files in " + s.getRoot());
                List<ParseResult<CompilationUnit>> results = s.tryToParse();
                results.stream().filter(p -> !p.isSuccessful()).forEach(p -> {
                    String problems = p.getProblems().stream().map(Problem::toString).collect(Collectors.joining());
                    logger.info(problems);
                });
                return s.getCompilationUnits().stream();
            }
            catch (IOException e) {
                logger.warning(e.getMessage());
                return Stream.empty();
            }
        });
    }
}
