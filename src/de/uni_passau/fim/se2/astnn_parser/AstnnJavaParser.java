package de.uni_passau.fim.se2.astnn_parser;

import java.io.File;
import java.nio.file.Path;
import java.util.concurrent.Callable;

import de.uni_passau.fim.se2.astnn_parser.pipeline.Pipeline;
import de.uni_passau.fim.se2.astnn_parser.pipeline.PipelineBuilder;
import picocli.CommandLine;
import picocli.CommandLine.Model.CommandSpec;
import picocli.CommandLine.ParameterException;

/**
 * Main entry point and commandline argument parser.
 */
public class AstnnJavaParser implements Callable<Integer> {

    @CommandLine.Spec
    CommandSpec spec;

    private Path sourceDirectory;

    private Path outputFile;

    public static void main(String[] args) {
        int exitCode = new CommandLine(new AstnnJavaParser()).execute(args);
        System.exit(exitCode);
    }

    @Override
    public Integer call() throws Exception {
        Pipeline p = PipelineBuilder.build();
        p.processDirectory(sourceDirectory, outputFile);
        return 0;
    }

    // @formatter:off
    @CommandLine.Option(
        names = { "-s", "--source" },
        description = "A directory containing Java source code.",
        required = true
    )
    // @formatter:on
    public void setSourceDirectory(final File sourceDirectory) {
        if (!sourceDirectory.exists() || !sourceDirectory.isDirectory()) {
            throw new ParameterException(spec.commandLine(), "The source directory must exist and be a directory.");
        }

        this.sourceDirectory = sourceDirectory.toPath();
    }

    // @formatter:off
    @CommandLine.Option(
        names = { "-o", "--output" },
        description = "The file in which the results should be written.",
        required = true
    )
    // @formatter:on
    public void setOutputFile(final File outputFile) {
        if (outputFile.exists() && outputFile.isDirectory()) {
            throw new ParameterException(spec.commandLine(), "Expected a file, not a directory for the output.");
        }

        this.outputFile = outputFile.toPath();
    }
}
