package fixtures;

import java.io.Serializable;
import java.util.List;

public class MissingConstructsExample {

    private final int x;
    private final int y;

    public MissingConstructsExample(int x) {
        this(x, x);
    }

    public MissingConstructsExample(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void iterateList(List<String> strings) {
        for (String string : strings) {
            System.out.println(string);
        }
    }

    private synchronized float foo(float f) {
        assert f > 0;
        float factor = 0.5f;
        return f == 1 ? (factor * f) : (factor / f);
    }

    float bar() {
        synchronized (this) {
            return foo((float) 42.23);
        }
    }

    void loop() {
        for (int i = 0; i < 42; i++) {
            for (int j = 0; j < 23; j++) {
                if (i * j % 1337 == 0) {
                    continue;
                }
                if (i + j == 65) {
                    break;
                }
            }
        }
    }

    void loop2() {
        for (int i = 0; i < 42; i++) {
            for (int j = 0; j < 23; j++) {
                if (i * j % 1337 == 0) {
                    continue foo;
                }
                if (i + j == 65) {
                    break foo;
                }
            }
        }
        foo:
        throw new Exception();;
    }

    void local() {
        class LocalClass {}
        LocalClass localClass = new LocalClass();

//        record Range(int lo, int hi) {
//            public Range {
//                if (lo > hi) {
//                    throw new IllegalArgumentException(String.format("(%d, %d)", lo, hi));
//                }
//            }
//        }
    }

    /* JavaParser does not seem to support this...
    int greet(int x) {
        int a = switch(x) {
            case 5,6:
                yield 20;
            default:
                yield 5+5;
        };
    }
     */

    int[][] arrayCreateExpr() {
        return new int[][]{{1},{2,3}};
    }

    Class<?> classExpr() {
        return MissingConstructsExample.class;
    }

    boolean instanceOf(Class<? extends MissingConstructsExample> c) {
        if (c instanceof Object) {
            return true;
        } else {
            return false;
        }
    }

//    boolean patternExpr(Object o) {
//        if (o instanceof String s) {
//            return true;
//        }
//        return false;
//    }

    String toString() {
        MissingConstructsExample.A.foo((Serializable & Clonable) this);
        return super.toString();
    }

    List<String> pipeline(List<Integer> values) {
        return values.stream().filter(x -> x > 0).map(Integer::toString).toList();
    }

    public static class A<T extends Serializable & Cloneable> {
        static void foo(T t) {
            try {
                t.clone();
            } catch (IllegalArgumentException | CloneNotSupportedException e) {

            }
        }
    }

    private void printList(List<?> c) {
        var a = 1;
        char c = 'a';
        long l = 1l;
        long j = 9223372036854775808L;
        Object o = null;
//        String s = """
//            Foo""";
    }

    private int[][] array() {
        return new int[1][2];
    }

}
