package fixtures;

public class EmptyConstructs {
    public static void emptyFor() {
        for (;;) {
            System.exit(1);
        }
    }

    public static void caselessSwitch() {
        switch (1) {
            // intentionally empty
        }
    }
}
