package de.uni_passau.fim.se2.astnn_parser;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import de.uni_passau.fim.se2.astnn_parser.model.ast.AstNode;
import de.uni_passau.fim.se2.astnn_parser.model.ast.AstnnNode;

public class TestUtils {

    public static AstnnNode walkTree(final AstnnNode node, int... childPositions) {
        AstnnNode current = node;
        for (int i : childPositions) {
            current = current.getChildren().get(i);
        }
        return current;
    }

    public static List<AstnnNode> getHeaderChildren(final AstNode node) {
        List<AstnnNode> children = node.getChildren();
        assertThat(children).isNotEmpty();
        return children.subList(0, children.size() - 1);
    }

    public static AstnnNode getBlockChild(final AstNode node) {
        assertThat(node.getChildren()).isNotEmpty();
        AstnnNode block = node.getChildren().get(node.getChildren().size() - 1);
        assertThat(block.getLabel()).isEqualTo(AstnnNode.BLOCK_LABEL);
        return block;
    }

    public static void assertChildrenLabels(final AstnnNode node, String... childrenLabels) {
        assertLabels(node.getChildren(), childrenLabels);
    }

    public static void assertLabels(final List<AstnnNode> nodes, String... labels) {
        assertThat(nodes.stream().map(AstnnNode::getLabel)).containsExactly(labels);
    }
}
